INTRODUCTION
------------

This tiny module ensures a persistent destination parameter on user login page.

Given the following use case:

You redirect anonymous users to the login page, when they try to access a page
they are not authorized to view and add the "destination" query argument in
order to send them back to that page after successful login,
e.g. www.example.com/user/login?destination=my-secret-page.

That only works well, as long the user only really immediately logs in. If
he/she needs to register first and follows the register link in the local tasks,
the destination parameter gets lost (www.example.com/user/register).

This module will alter the local task links on the user login and register pages
(that are usually login, register and password reset) and preserves the
"destination" parameter, if present.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the user links destination from appearing.

MAINTAINERS
-----------

Current maintainers:
 * Mahtab Alam (mahtabalam) - https://www.drupal.org/u/mahtabalam

This project has been sponsored by:
 * Hureka Technologies Inc. (https://www.drupal.org/hureka-technologies-inc)
